Hello (AWS) Lambda with Terraform
=================================


This project is an example of a Python (AWS)
[Lambda](https://aws.amazon.com/lambda/) exposed with [API
Gateway](https://aws.amazon.com/api-gateway/), configured with
[Terraform](https://www.terraform.io/). 
Lambda function execute python code stored into a [s3 bucket](https://aws.amazon.com/fr/s3/).


![](doc/big-picture.png)


## Introduction

This demo project creates a `/hello` endpoint with two methods (`GET` and
`POST`). Both methods are bound to a **single file** containing two handlers
(a.k.a. lambda functions, one for each method). This is defined by a `handler`
parameter. The code for each lambda function is written in Python (method names
are just a convention):

```python
def handler(event, context):
    return { "message": "Hello World, my name is loca and u ?!" }

def post_handler(event, context):
    return { "message": "I should have created something my friends..." }
```

The [Terraform configuration](main.tf) relies on 4 modules:
[`lambda`](../lambda/), [`api_method`](../api_method/), [`S3_python_function_code`](../api_method/) and
[`s3 python function code`](../S3_python_function_code/). See the [Terraform Modules
section](#terraform-modules) for further information. This configuration creates
a remote tfstate on a s3 bucket, and manage concurrent access with dynamoDB. Another bucket is created to store
lambda function  code. And creates two lambda functions on AWS Lambda, a (deployed) REST API with a single endpoint
and two HTTP methods on API Gateway, and takes care of the permissions and
credentials. 
The figure below is an example of what you get in the API Gateway
dashboard:

![](doc/hello.png)


## Getting started

You must have an [AWS account](http://aws.amazon.com/). Next, you must [install
Terraform](https://www.terraform.io/intro/getting-started/install.html) first.

Clone this repository, then run:

    $ mv terraform.tfvars.example terraform.tfvars

You have to configure your aws cli account :
    
    $ aws configure 
    
You need to comment backend for the first time in [provider file](providers.tf)

```
//terraform {
//  backend "s3" {
//    bucket         = "com.example.dev.terraform"
//    key            = "terraform.tfstate"
//    region         = "eu-west-1"
//    dynamodb_table = "terraform_dev"
//  }
//}
```

You are now ready to use Terraform!

    $ terraform init

If everything is OK, you can build the whole infrastructure:

    $ terraform apply

For switch to the remote terraform state, we need to uncomment backend in [provider file](providers.tf)

```
terraform {
  backend "s3" {
    bucket         = "com.example.dev.terraform"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "terraform_dev"
  }
}
```
And run:

    $ terraform init
For initialize your backend to the remote tfstate.

You can destroy all the components by running:

    $ terraform destroy
    
To use gitlab ci, you need to enter some variables into "settings" -> "ci / cd" -> "Variables"



For more information, please read [the Terraform
documentation](https://www.terraform.io/docs/index.html).


## Archi

![](doc/archi.png)
![](doc/archi-ops.png)

To force the reload of the lambda, we need to add [source_hash_code](https://www.terraform.io/docs/providers/aws/r/lambda_function.html#source_code_hash) field to the lambda.
Permit to change the hash of the source code every time and force the reload with terraform.

