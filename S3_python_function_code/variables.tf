variable "name" {
  description = "The name of the bucket to create"
}
variable "tags_name" {
  description = "The tags name is visible into tfstate permit to add more informations"
}
variable "environment" {
  description = "The tag environnement is visible into tfstate permit to add more informations"
}
variable "access" {
  description = "You can set the access to private or public-read or log-delivery-write ..."
}
variable "path_file_name" {
  description = "locale path to your code to upload to s3"
}
variable "function_name" {
  description = "Function name for trigger it when actions is made on the bucket"
}
