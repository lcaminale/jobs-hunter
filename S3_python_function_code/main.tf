# S3 bucket to store python function code for the lambda
resource "aws_s3_bucket" "functions_bucket" {
  bucket = "${var.name}"
  acl    = "${var.access}"
  force_destroy = true

  tags = {
    Name        = "${var.tags_name}"
    Environment = "${var.environment}"
  }
}

# S3 bucket object to add an object to the bucket, here the zip code
resource "aws_s3_bucket_object" "hello_lambda_function" {
  bucket = "${aws_s3_bucket.functions_bucket.id}"
  key    = "hello_lambda.zip"
  source = "${var.path_file_name}"
}

output "name" {
  value = "${aws_s3_bucket.functions_bucket.bucket}"
}

output "aws_s3_bucket_object_etag" {
  value = "${aws_s3_bucket_object.hello_lambda_function.etag}"
}

