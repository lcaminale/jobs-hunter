
# First, we need a role to play with Lambda
resource "aws_iam_role" "iam_role_for_lambda" {
  name = "iam_role_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


# s3 bucket to store python function code
module "function_code_bucket" {
  source          = "./S3_python_function_code"
  name            = "python-function-code-bucket"
  tags_name       = "python function code bucket"
  environment     = "Dev"
  access          = "private"
  path_file_name  = "hello_lambda.zip"
  function_name   = "${module.lambda.arn}"
}

# Here is a first lambda function that will run the code `hello_lambda.handler`
module "lambda" {
  source                    = "./lambda"
  name                      = "hello_lambda"
  runtime                   = "python2.7"
  role                      = "${aws_iam_role.iam_role_for_lambda.arn}"
  bucket_name               = "${module.function_code_bucket.name}"
  aws_s3_bucket_object_etag = "${module.function_code_bucket.aws_s3_bucket_object_etag}"
}

# This is a second lambda function that will run the code
# `hello_lambda.post_handler`
module "lambda_post" {
  source                    = "./lambda"
  name                      = "hello_lambda"
  handler                   = "post_handler"
  runtime                   = "python2.7"
  role                      = "${aws_iam_role.iam_role_for_lambda.arn}"
  bucket_name               = "${module.function_code_bucket.name}"
  aws_s3_bucket_object_etag = "${module.function_code_bucket.aws_s3_bucket_object_etag}"
}

# Now, we need an API to expose those functions publicly
resource "aws_api_gateway_rest_api" "hello_api" {
  name = "Hello API"
}

# The API requires at least one "endpoint", or "resource" in AWS terminology.
# The endpoint created here is: /hello
resource "aws_api_gateway_resource" "hello_api_res_hello" {
  rest_api_id = "${aws_api_gateway_rest_api.hello_api.id}"
  parent_id   = "${aws_api_gateway_rest_api.hello_api.root_resource_id}"
  path_part   = "hello"
}

# Until now, the resource created could not respond to anything. We must set up
# a HTTP method (or verb) for that!
# This is the code for method GET /hello, that will talk to the first lambda
module "hello_get" {
  source      = "./api_method"
  rest_api_id = "${aws_api_gateway_rest_api.hello_api.id}"
  resource_id = "${aws_api_gateway_resource.hello_api_res_hello.id}"
  method      = "GET"
  path        = "${aws_api_gateway_resource.hello_api_res_hello.path}"
  lambda      = "${module.lambda.name}"
  region      = "${var.aws_region}"
  account_id  = "${var.aws_account_id}"
}

# This is the code for method POST /hello, that will talk to the second lambda
module "hello_post" {
  source      = "./api_method"
  rest_api_id = "${aws_api_gateway_rest_api.hello_api.id}"
  resource_id = "${aws_api_gateway_resource.hello_api_res_hello.id}"
  method      = "POST"
  path        = "${aws_api_gateway_resource.hello_api_res_hello.path}"
  lambda      = "${module.lambda_post.name}"
  region      = "${var.aws_region}"
  account_id  = "${var.aws_account_id}"
}

# We can deploy the API now! (i.e. make it publicly available)
resource "aws_api_gateway_deployment" "hello_api_deployment" {
  rest_api_id = "${aws_api_gateway_rest_api.hello_api.id}"
  stage_name  = "production"
  description = "Deploy methods: ${module.hello_get.http_method} ${module.hello_post.http_method}"
}

# When you are in team developpers you need to share your tfstate and make lock on it
# This module will create a bucket to store the tfstate and a dynamoDB to lock this file when someone write on it
module "dev-tfstate" {
  source = "./terraform_state_s3"
  env = "dev"
  s3_bucket = "com.example.dev.terraform"
  s3_bucket_name = "Dev Terraform State Store"
  dynamodb_table = "terraform_dev"
}

# We can use a remote state on s3 :D
terraform {
  backend "s3" {
    bucket         = "com.example.dev.terraform"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "terraform_dev"
  }
}

