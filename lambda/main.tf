# Template for lambda aws, the python function code is on a s3 bucket
# We need source_code_hash to reload the the function
resource "aws_lambda_function" "lambda" {
  s3_bucket        = "${var.bucket_name}"
  s3_key           = "${var.name}.zip"
  function_name    = "${var.name}_${var.handler}"
  role             = "${var.role}"
  handler          = "${var.name}.${var.handler}"
  runtime          = "${var.runtime}"
  source_code_hash = "${var.aws_s3_bucket_object_etag}"
}

output "arn" {
  value = "${aws_lambda_function.lambda.arn}"
}

output "name" {
  value = "${aws_lambda_function.lambda.function_name}"
}
