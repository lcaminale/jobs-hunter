variable "aws_account_id" {
  description = "AWS account id"
  type        = "string"
}

variable "aws_access_key" {
  description = "AWS access key"
}

variable "aws_secret_key" {
  description = "AWS secret key"
}

variable "aws_region" {
  description = "AWS region"
  default     = "eu-west-1"
}

variable "terraform-state-bucket-name" {
  description = "bucket name for storing tfstate"
  default     = "terraform-remote-state-storage-s3-bucket"
}

variable "dynamodb-table-name" {
  description = "Dynamo db name for secure concurrent write/acces on tfstate"
  default     = "terraform-state-lock-dynamo"
}



